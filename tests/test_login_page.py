# Open browser
# Go to webpage
# selenium 4
import time

import pytest
from selenium import webdriver  # import selenium library
from selenium.webdriver.chrome.service import Service as ChromeService # import selenium service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager  # installs chrome driver


class TestPositiveScenarios:
    @pytest.mark.login
    @pytest.mark.positive
    def test_positive_login(self):

        driver= webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
        time.sleep(5)

        # Open Webpage
        driver.get("https://practicetestautomation.com/practice-test-login/")  # necessary to open the page under test
        time.sleep(5)


        # type username student into username
        username_locator = driver.find_element(By.ID, "username")
        username_locator.send_keys("student")

        # type password into  password locator
        password_locator = driver.find_element(By.NAME, "password")
        password_locator.send_keys("Password123")

        # submit button //[@attribute='value']
        submit_button_locator = driver.find_element(By.XPATH, "//button[@class='btn']")
        time.sleep(2)
        submit_button_locator.click()
        time.sleep(2)

        # Verify new page URL contains  practicetestautomation.com/logged
        test_actual_url= driver.current_url  # current url is a property of driver property
        assert test_actual_url == "https://practicetestautomation.com/logged-in-successfully/"

        # Verify new page contains expected text
        text_locator = driver.find_element(By.TAG_NAME, "h1")
        actual_text =text_locator.text
        assert actual_text == "Logged In Successfully"

        # Verify button log out is displayed on the new page
        log_out_button_locator = driver.find_element(By.LINK_TEXT, "Log out")
        assert log_out_button_locator.is_displayed()

        # new_url_locator = driver.find_element(By.LINK_TEXT, "canonical")



        """
        Open the page
        Type username student into username field
        Type password into password field
        Push Submit button
        Verify new page URL contains practicetestautomation.com/logged
        Verify new page contains expected class
        Verify button log out is displayed on the new page
        
        
        
        """