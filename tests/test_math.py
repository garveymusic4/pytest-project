import pytest


def add_two_numbers(a, b):
    return a + b

@pytest.mark.math
def test_small_numbers():
    assert add_two_numbers(6, 6) == 12, "the sum of the 5 and 6 should be 11"

@pytest.mark.math
def test_large_numbers():
    assert add_two_numbers(100, 300) == 400, "the sum of 100 and 3000 should be 400"
